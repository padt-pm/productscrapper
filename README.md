## How to install prerequisites

### Leclerc Drive Lublin
`pip install -r requirements.txt` to install requirements.


### Frisco
Download Chrome driver from https://sites.google.com/a/chromium.org/chromedriver/downloads and place in PATH or cwd.

`pip install -r requirements.txt` to install requirements.



## How to use

### Leclerc Drive Lublin

`scrapy crawl leclerc_drive_lublin` to start scraping.

Default categories used are 'WODA I NAPOJE' and 'DROGERIA'.

### Frisco
`scrapy crawl frisco` to start scraping.

### Leclerc Drive Lublin, Frisco - extra parameters

`scrapy crawl <scraper-name> -O output.json` to save output to file `output.json`.

You can define a case-invariant, semicolon-separated list of categories to scrape. Shop category will be scraped if any of the searched categories (argument) is a substring of the shop category. Categories are scraped recursively - if a category is scraped, its subcategories will be scraped as well.

Previous commands are equivalent to launching scraper with `category` parameter, e.g:

`scrapy crawl <scraper-name> -a category="Woda i napoje;Drogeria"`

`scrapy crawl <scraper-name> -a category=woda`

`scrapy crawl <scraper-name> -a category="Woda i napoje"`

`scrapy crawl <scraper-name> -a category=woda;drogeria`

You can force perfect match (still case-invariant) on category name by setting `perfect_match` parameter, e.g:

`scrapy crawl <scraper-name> -a category=Drogeria -a perfect_match=true`

### Allegro
Allegro scrapper contains two modes of work:

1. scraping whole categories and subcategories
2. scraping data based on jsons with EANs

#### Usage
1. `scrapy crawl allegro -a category=supermarket` to start scraping given category  
You can define a case-invariant, semicolon-separated list of categories to scrape.  
Shop category will be scraped if any of the searched categories (argument) is a substring of the shop category.  
Categories are scraped recursively - if a category is scraped, its subcategories will be scraped as well.  
Examples:  
  
    * `scrapy crawl allegro -a category=supermarket;firma`  
    * `scrapy crawl allegro -a category=super;moda`  
    * `scrapy crawl allegro -a category="Supermarket;Moda;Zdrowie"`  

2. `scrapy crawl allegro` to start scraping data based on supplied jsons  
Given jsons with product EANs (in a column "EAN"), scraper will scrape all products with matching EANs from Allegro.  
All jsons must be inside an already existing folder "jsons" located in main directory.  
To save downloaded data to json, add `-O output.json` to the end of command to save output to file `output.json`.  
Examples:  
  
    * `scrapy crawl allegro -a category=supermarket;firma -O supermarket_and_firmy.json`
    to save all scraped data
    from "Supermarket" and "Firma" categories into a `supermarket_i_firma.json` file
    * `scrapy crawl allegro -O output.json` to save all scraped data from given jsons into a `output.json` file
