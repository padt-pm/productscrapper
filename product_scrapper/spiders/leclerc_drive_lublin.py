import scrapy
import re

class LeclercDriveLublinSpider(scrapy.Spider):
    name = 'leclerc_drive_lublin'
    allowed_domains = ['leclercdrive.lublin.pl']
    start_urls = ['https://leclercdrive.lublin.pl/']
    searched_categories = ['woda i napoje', 'drogeria'] # replacable by spider argument

    subcategories_selector = '#subcategories .subcategory-image a'
    product_selector = '.product_list .product-container .product-image-container a'
    next_page_selector = '.top-pagination-content li.pagination_next:not(.disabled) a'


    def __init__(self, category = None, perfect_match = False, *args, **kwargs):
        self.perfect_match = perfect_match.lower() == 'true'

        if category is not None:
            self.searched_categories = category.split(';')

        super().__init__(*args, **kwargs)


    def parse(self, response):
        categories_container = response.css('div.sf-contener')

        def get_categories(response, container):
            requests = []
            category_container = container.xpath('ul/li')

            for category_selector in category_container:
                category_name = category_selector.xpath('a/text()').extract_first()

                if category_name.lower() == 'szybkie zakupy':
                    # avoid duplicates
                    continue

                if not self.perfect_match:
                    # any of the searched categories is a substring of this category
                    search_category_matches = [c.lower() in category_name.lower() for c in self.searched_categories]
                else:
                    # any of the searched categories matches this category
                    search_category_matches = [c.lower() == category_name.lower() for c in self.searched_categories]

                if True in search_category_matches:
                    href = category_selector.xpath('a/@href').extract_first()
                    new_category_request = response.follow(href, callback=self.parse_category_recursively) # entry point for category parsing
                    requests.append(new_category_request)

                    # crawling recursively, so subcategories won't be needed
                    continue
                
                # check subcategories
                requests += get_categories(response, category_selector)
            
            return requests

        yield from get_categories(response, categories_container)


    def parse_category_recursively(self, response):
        old_category = response.meta['category'] + ' > ' if 'category' in response.meta else ''
        subcategory_name = response.css('span.cat-name::text').get().replace('\u00a0', ' ').replace(' +', ' ').strip()
        category = old_category + subcategory_name
        response.meta['category'] = category

        subcategories = response.css(self.subcategories_selector)
        yield from response.follow_all(subcategories, self.parse_category_recursively, meta = { 'category': category })
        
        yield from self.parse_result_page(response)


    def parse_result_page(self, response):
        category = response.meta['category']

        has_product_list = response.css('.heading-counter::text').get() is not None
        if has_product_list:
            products = response.css(self.product_selector)
            yield from response.follow_all(products, self.parse_product, meta = { 'category': category })

        pagination_links = response.css(self.next_page_selector)
        yield from response.follow_all(pagination_links, self.parse_result_page, meta = { 'category': category })


    def parse_product(self, response):
        def extract_with_css(query):
            return response.css(query).get(default='').strip()

        categories = response.css('.breadcrumb a::attr(title)').getall()

        result = {
            'EAN': re.match('.*-([0-9]+).html$', response.url).group(1),
            'Nazwa': extract_with_css('div.primary_block .container h1::text'),
            'Url': response.url,
            'Cena': extract_with_css('#our_price_display::text'),
            'Ilość': extract_with_css('.weight .unity::text'),
            'Cena jednostkowa': extract_with_css('.unit-price::text'),
            'Kategoria': ' > '.join(categories[1:]), # visible at product page
            'Ścieżka kategorii': response.meta['category'], # the path that scaper took to arrive here
            'Url obrazka': extract_with_css('.pb-left-column #image-block img::attr(src)')
        }

        attributes_selector = '#featured ul.ui-tabs-nav li.ui-tabs-nav-item a'
        attributes = zip(response.css(attributes_selector + '::text').getall(), response.css(attributes_selector + '::attr(href)').getall())

        for name, id in attributes:
            attr_text = response.css(f'{id}::text').extract()
            attr_text = ''.join(attr_text).strip()
            
            result[name] = attr_text
        
        yield result
