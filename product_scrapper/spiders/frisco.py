import scrapy, time, json, os
from bs4 import BeautifulSoup

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

class FriscoSpider(scrapy.Spider):
    name = 'frisco'
    allowed_domains = ['frisco.pl']
    home_page = 'https://www.frisco.pl'
    start_urls = [home_page]
    searched_categories = ['napoje', 'środki czystości, chemia'] # replacable by spider argument

    SCROLL_PAUSE_TIME = 0.5


    def __init__(self, category = None, perfect_match = 'false', *args, **kwargs):
        if perfect_match:
            self.perfect_match = perfect_match.lower() == 'true'

        if category is not None:
            self.searched_categories = category.split(';')

        super().__init__(*args, **kwargs)


    def get_product_urls_from_tags(self, product_tags):
        urls = []
        for product in product_tags:
            product_labels = product.find_element_by_css_selector('.product-box_content div.product-box_labels')
            is_recommended = 'Produkt polecany' in product_labels.text

            if is_recommended:
                continue

            product_link = product.find_element_by_css_selector('.product-box_content a.product-box_image')
            url = product_link.get_attribute('href')
            urls.append(url)

        return urls


    def get_all_product_urls(self, driver):
        wait = WebDriverWait(driver, 15)
        wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.product-box')))
        wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.product-box .product-box_content div.product-box_labels')))

        products = driver.find_elements_by_css_selector('.product-box')
        urls = self.get_product_urls_from_tags(products)

        last_height = driver.execute_script("return window.scrollY")

        while True:
            # Scroll down
            driver.execute_script("window.scrollTo(0, window.scrollY + 400);")

            # Wait to load page
            time.sleep(self.SCROLL_PAUSE_TIME)
            products = driver.find_elements_by_css_selector('.product-box')
            urls += self.get_product_urls_from_tags(products)

            # Check if scroll increased
            new_height = driver.execute_script("return window.scrollY")
            if new_height == last_height:
                break
            last_height = new_height

        def unique(list):
            newlist = []
            for e in list:
                if e not in newlist:
                    newlist.append(e)
            return newlist
        
        unique_urls = unique(urls)
        print(f'{len(urls)} links checked, {len(unique_urls)} unique products found')

        return unique_urls


    def get_category_urls(self, driver):
        driver.get(self.home_page)

        driver.find_element_by_css_selector('.horizontal-navigation-bar_categories a')\
            .click()

        wait = WebDriverWait(driver, 15)
        main_categories_list = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.categories-dropdown .main-categories')))
        categories = main_categories_list.find_elements_by_css_selector('a')

        urls = []

        selectors = [
            '.categories-dropdown .main-categories', # categories
            '.categories-dropdown .subcategories .subcategories-inner', # subcategories
            '.categories-dropdown .subcategories .subsubcategories', # subsubcategories
        ]

        def get_urls(categories, level):
            for category in categories:
                if not self.perfect_match:
                    # any of the searched categories is a substring of this category
                    search_category_matches = [c.lower() in category.text.lower() for c in self.searched_categories]
                else:
                    # any of the searched categories matches this category
                    search_category_matches = [c.lower() == category.text.lower() for c in self.searched_categories]

                if True in search_category_matches:
                    urls.append(category.get_attribute('href'))
                    # crawling recursively, so subcategories won't be needed
                    continue
                
                if level < 3:
                    hover = ActionChains(driver).move_to_element(category)
                    hover.perform()

                    subcategories_list = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR,
                        selectors[level])))
                    subcategories = subcategories_list.find_elements_by_css_selector('a')

                    get_urls(subcategories, level + 1)

        get_urls(categories, level = 1)
        return urls

    
    def parse(self, response):
        options = webdriver.ChromeOptions()
        options.headless = True
        options.add_argument("window-size=1920,1080")
        driver = webdriver.Chrome(options=options)

        start_urls = self.get_category_urls(driver)

        urls = []
        for category_url in start_urls:
            driver.get(category_url)
            urls += self.get_all_product_urls(driver)

        urls = list(set(urls))
        with open('frisco-urls.json', 'w') as f:
            json.dump(urls, f)

        for url in urls: # ['https://www.frisco.pl/pid,126695/stn,product']:
            url = url.replace("'", "\\'")
            driver.execute_script(f"window.open('{url}', '_blank')")
            driver.switch_to.window(driver.window_handles[1])
            
            yield from self.scrape_product(driver)

            driver.close()
            driver.switch_to.window(driver.window_handles[0])
    
    def to_selector(self, driver):
        page_source = driver.page_source
        selector = scrapy.Selector(text = page_source)
        return selector

    def scrape_product(self, driver):
        results = [self.scrape_page(driver)]
        while True:
            try:
                next_variant = driver.find_element_by_css_selector('.product-page_inf .product-page_multipack-switcher a.active + a')
                driver.execute_script("arguments[0].scrollIntoView();", next_variant)
                driver.execute_script("arguments[0].click();", next_variant)
                results.append(self.scrape_page(driver))
            except:
                return results


    def scrape_page(self, driver):
        result = { }

        wait = WebDriverWait(driver, 60)
        name_box = wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, '.product-page_info .product-page_name-box')))
        
        result['Url'] = driver.current_url

        def parse_name_box(name_box):
            name = name_box.find_element_by_css_selector('h1')
            result['Nazwa'] = name.text

            subname = name_box.find_element_by_css_selector('h3')
            result['Podnazwa'] = subname.text

            meta = name_box.find_elements_by_css_selector('meta')
            for meta_tag in meta:
                result[meta_tag.get_attribute('itemprop')] = meta_tag.get_attribute('content')

        parse_name_box(name_box)

        category_info = driver.find_element_by_css_selector('.breadcrumbs')

        def parse_category_info(category_info):
            soup = BeautifulSoup(category_info.get_attribute('innerHTML'), 'html.parser')
            result['Kategoria'] = ' > '.join([tag.get_text() for tag in soup.find_all(recursive = False)][1:])
        
        parse_category_info(category_info)

        image = driver.find_element_by_css_selector('.product-page_image img')
        result['Url obrazka'] = image.get_attribute('src')

        product_info = driver.find_element_by_css_selector('.product-page_info .product-page_inf')

        def parse_product_info(product_info):
            quantity = product_info.find_element_by_css_selector('.product-page_labels .product-page_labels-single.green')
            result['Ilość'] = quantity.text

            try:
                validity_period = product_info.find_element_by_css_selector('.product-box_additional-info .product-box_validity-period')
                result['Data ważności'] = validity_period.text
            except:
                pass

            cart_box = product_info.find_element_by_css_selector('.product-page_cart-box2')

            def parse_cart_box(cart_box):
                meta = cart_box.find_elements_by_css_selector('div.cart-box > meta')
                for meta_tag in meta:
                    result[meta_tag.get_attribute('itemprop')] = meta_tag.get_attribute('content')

                hidden = cart_box.find_elements_by_css_selector('div.cart-box > span')
                for hidden_tag in hidden:
                    result[hidden_tag.get_attribute('itemprop')] = hidden_tag.get_attribute("textContent")
                
                price = cart_box.find_element_by_css_selector('div.cart-box > .cart-box_frame > .cart-box_price')
                
                def parse_price(price):
                    price_meta = price.find_elements_by_css_selector('meta')
                    for meta_tag in price_meta:
                        result[meta_tag.get_attribute('itemprop')] = meta_tag.get_attribute('content')
                    
                    new_price = price.find_element_by_css_selector('.main-price')
                    result['new_price'] = new_price.text

                    old_price = price.find_elements_by_css_selector('.product-box_old-price')
                    if len(old_price) > 0:
                        old_price = old_price[0]
                        result['old_price'] = old_price.text
                
                parse_price(price)

                desc_section = cart_box.find_element_by_css_selector('div.cart-box > .cart-box_frame > .desc-section')
                
                def parse_desc_section(desc_section):
                    desc_items = desc_section.find_elements_by_css_selector('.desc')
                    for i, desc_item in enumerate(desc_items):
                        soup = BeautifulSoup(desc_item.get_attribute('innerHTML'), 'html.parser')
                        result[f'desc_item{i}'] = soup.get_text()

                parse_desc_section(desc_section)
                
                availability = cart_box.find_element_by_css_selector('div.cart')      
                result[availability.get_attribute('itemprop')] = availability.get_attribute('content')
                            
            parse_cart_box(cart_box)

            short_description = product_info.find_element_by_css_selector('.product-page_short-desc')

            def parse_short_description(short_description):
                attributes = short_description.find_elements_by_css_selector('div')

                for attr in attributes:
                    name = attr.find_element_by_css_selector('.what')
                    value = attr.find_element_by_css_selector('.value')

                    name_text = name.text[:-1] if name.text.endswith(':') else name.text

                    if name_text == 'Tagi':
                        soup = BeautifulSoup(value.get_attribute('innerHTML'), 'html.parser')
                        result[name_text] = ', '.join([tag.get_text() for tag in soup.find_all(recursive = False)])
                    else:
                        result[name_text] = value.text
            
            parse_short_description(short_description)

        parse_product_info(product_info)

        product_details = driver.find_element_by_css_selector('.product-page_details')

        def parse_product_details(product_details):
            other_tabs = product_details.find_elements_by_css_selector('.ui-tabs_header .ui-tabs_tab:not(.active)')
            
            def parse_details_tab(product_details):
                selected_tab = product_details.find_element_by_css_selector('.ui-tabs_header .ui-tabs_tab.active')

                selector = self.to_selector(driver)
                tab_content = selector.css('.ui-tabs_tab-content > div > *')

                tab_tags = tab_content.extract()

                def parse_tab_tags(current_section, tab_tags):
                    soup = BeautifulSoup('\n'.join(tab_tags), 'html.parser')

                    all_tags = soup.find_all(recursive = False)
                    section_content = ''

                    for tag in all_tags:
                        if tag.name == 'h3':
                            if section_content != '':
                                result[current_section] = section_content.strip()
                            current_section = tag.string
                            section_content = ''
                        elif tag.name == 'p':
                            section_content += f'{tag.get_text()}\n'
                        elif tag.name == 'br':
                            section_content += '\n'
                        elif tag.name == 'table':
                            section_content += '\n'
                            for table_row in tag.find_all('tr'):
                                columns = table_row.find_all('th') + table_row.find_all('td')
                                n = len(columns)
                                for i, column in enumerate(columns):
                                    txt = column.text.replace('"', '\'')
                                    section_content += f'"{txt}"'
                                    if i != n - 1:
                                        section_content += ','
                                section_content += '\n'
                            section_content += '\n'
                        elif tag.name == 'th' or tag.name == 'tr' or tag.name == 'td' or tag.name == 'tbody':
                            continue
                        elif tag.name == 'h1' or tag.name == 'h2' or tag.name == 'h4' or tag.name == 'h6' or tag.name == 'h6':
                            section_content += f'{tag.get_text()}\n'
                        elif tag.get_text() is not None:
                            section_content += f'{tag.get_text()} '
                        else:
                            continue
                    
                    if section_content != '':
                        result[current_section] = section_content.strip()

                parse_tab_tags(selected_tab.text, tab_tags)
            
            parse_details_tab(product_details)

            for tab in other_tabs:
                driver.execute_script("arguments[0].scrollIntoView();", tab)
                driver.execute_script("arguments[0].click();", tab)
                parse_details_tab(product_details)

        parse_product_details(product_details)

        return result
