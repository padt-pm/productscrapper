import scrapy
import re
import json
import time
import os


def get_links(path_to_json):
    with open(path_to_json) as products:
        return [
            f"https://allegro.pl/listing?string={row['EAN']}&bmatch=baseline-product-cl-eyesa2-engag-dict45-uni-1-5-1103"
            for row in json.load(products)]


class AllegroSpider(scrapy.Spider):
    name = 'allegro'
    allowed_domains = ['allegro.pl']
    folder_with_jsons = "jsons"
    start_urls = ['https://allegro.pl']
    searched_categories = None
    custom_settings = {
        'CONCURRENT_REQUESTS': '4',
        'CONCURRENT_REQUESTS_PER_DOMAIN': '4',
    }

    product_selector = '/html/body/div[2]/div[4]/div/div/div/div/div/div[2]/div[1]/div[3]/div[1]/div/div/div[2]/div[' \
                       '1]/div/section/div/article/div/div[1]/div/a '
    top2sponsored_product_selector = '._9c44d_3pyzl > article > div:nth-child(1) > div:nth-child(1) > div:nth-child(' \
                                     '1) > a:nth-child(1)::attr(href) '
    sponsored_product_selector = '._9c44d_3pyzl > div > article:nth-child(1) > div:nth-child(1) > div:nth-child(1) > ' \
                                 'div:nth-child(1) > a:nth-child(1)::attr(href) '
    next_page_selector = 'div._np6in:nth-child(2) > a:nth-child(2)'
    subcategory_selector = 'li._xu6h2 > a:nth-child(1)::attr(href)'
    category_name_selector = 'div.mwfq_6m > a:nth-child(1)::text'
    category_link_selector = 'div.mwfq_6m > a:nth-child(1)::attr(href)'


    def __init__(self, category=None, *args, **kwargs):
        if category is not None:
            self.searched_categories = category.split(';')

        super().__init__(*args, **kwargs)

    def parse_recursively(self, response):
        possible_subcategories = [f'https://allegro.pl{subpage}' for subpage in
                                  response.css(self.subcategory_selector).getall()]
        if len(possible_subcategories) == 0:
            yield from response.follow_all([response.request.url], self.parse_list_of_products, dont_filter=True)
        else:
            yield from response.follow_all(possible_subcategories, self.parse_recursively)

    def parse(self, response):
        if self.searched_categories is None:
            list_of_products = []
            product_lists_urls = []
            if os.path.exists(self.folder_with_jsons):
                for f in os.listdir(self.folder_with_jsons):
                    path_to_file = os.path.join(self.folder_with_jsons, f)
                    if os.path.isfile(path_to_file) and path_to_file.endswith(".json"):
                        list_of_products.append(get_links(path_to_file))
                product_lists_urls = [item for sublist in list_of_products for item in sublist]
            yield from response.follow_all(product_lists_urls, self.parse_list_of_products)

        else:
            category_names = [c.lower() for c in response.css(self.category_name_selector).getall()]
            category_links = [link.split('/')[-1] for link in
                              response.css(self.category_link_selector).getall()]
            category_names = category_names[:-2]  # removing eBilet and AllegroLokalnie
            category_links = category_links[:-2]  # removing eBilet and AllegroLokalnie
            category_dict = dict(zip(category_names, category_links))

            search_category_matches = []
            for item in self.searched_categories:
                matching_names = [s for s in category_names if item.lower() in s]
                for name in matching_names:
                    if name not in search_category_matches:
                        search_category_matches.append(name)

            selected_categories = [f"https://allegro.pl/kategoria/{category_dict[name]}?string=*" for name in
                                   search_category_matches]
            yield from response.follow_all(selected_categories, self.parse_recursively)

    def parse_list_of_products(self, response):
        # normal offers
        products = response.xpath(self.product_selector)
        product_links = [f"{link.attrib['href']}#productReviews" for link in products]

        # top 2 sponsored offers
        products_sponsored = response.css(self.top2sponsored_product_selector).getall()
        for product in products_sponsored:
            product_links.append(f"{product}#productReviews")

        # other sponsored offers
        products_sponsored_2 = response.css(self.sponsored_product_selector).getall()
        for product in products_sponsored_2:
            product_links.append(f"{product}#productReviews")

        time.sleep(5)
        match = re.match('.*string=([0-9]+)&.*', response.url)
        EAN = None
        if match is not None:
            EAN = match.group(1)
        yield from response.follow_all(product_links, self.parse_product, meta={'EAN': EAN})

        pagination = response.css(self.next_page_selector)
        pagination_links = [link.attrib['href'] for link in pagination]
        yield from response.follow_all(pagination_links, self.parse_list_of_products)

    def parse_product(self, response):
        categories = response.css('html body div.main-wrapper div '
                                  'div.opbox-sheet-wrapper.m7er_k4.munh_56.m3h2_56._26e29_2AYAm.mjru_k4 '
                                  'div.opbox-sheet._26e29_11PCu div '
                                  'div.opbox-sheet-wrapper.m7er_k4.munh_56.m3h2_56._26e29_2AYAm.mjru_ey '
                                  'div.opbox-sheet._26e29_11PCu.card._9f0v0.msts_n7 div '
                                  'div._1yyhi._e8a20_1p4TL._e8a20_3TA2g '
                                  'div._3kk7b.myre_zn._e8a20_CWOtz._vnd3k._fdr6w.mryx_16 div div._1wvj0 '
                                  'div._1jnz0._e7tha._d2756_1mE2L div._17qy1._bxr46._d2756_GNE_S '
                                  'a._1liky._w7z6o._uj8z7._jmjqf._d2756_1wB6R span::text').getall()

        title = response.css('._9a071_1Ux3M::text').extract_first()

        price = response.css('div._1svub::attr(aria-label)').extract_first()


        price = float(price[5:-6].replace("zł", ".").replace(" ", ""))

        keys = response.css('html body div.main-wrapper div '
                            'div.opbox-sheet-wrapper.m7er_k4.munh_56.m3h2_56._26e29_2AYAm.mjru_k4 '
                            'div.opbox-sheet._26e29_11PCu div '
                            'div.opbox-sheet-wrapper.m7er_k4.munh_56.m3h2_56._26e29_2AYAm.mjru_ey '
                            'div.opbox-sheet._26e29_11PCu.card._9f0v0.msts_n7 div ul._3a4zn._1sql3._1rj80._1qltd '
                            'li._1bmp9 div._1h7wt._1bo4a._1fwkl._f8818_2MnBl '
                            'div._1bo4a._1fwkl._f8818_2L1Cr._f8818_2JUs8 ul._1rj80._1sql3._1qltd._f8818_1uDuE '
                            'li._f8818_2jDsV div._1qltd._f8818_3-1jj div._17qy1._1vryf._f8818_1X1F-::text').getall()
        clean_keys = [key for key in keys if key != ":"]

        clean_values = response.css('html body div.main-wrapper div '
                                    'div.opbox-sheet-wrapper.m7er_k4.munh_56.m3h2_56._26e29_2AYAm.mjru_k4 '
                                    'div.opbox-sheet._26e29_11PCu div '
                                    'div.opbox-sheet-wrapper.m7er_k4.munh_56.m3h2_56._26e29_2AYAm.mjru_ey '
                                    'div.opbox-sheet._26e29_11PCu.card._9f0v0.msts_n7 div '
                                    'ul._3a4zn._1sql3._1rj80._1qltd li._1bmp9 div._1h7wt._1bo4a._1fwkl._f8818_2MnBl '
                                    'div._1bo4a._1fwkl._f8818_2L1Cr._f8818_2JUs8 ul._1rj80._1sql3._1qltd._f8818_1uDuE '
                                    'li._f8818_2jDsV div._1qltd._f8818_3-1jj div._17qy1._f8818_DQKcc').xpath('string('
                                                                                                             '.)').extract()

        EAN = response.meta.get('EAN')

        result = {
            'EAN': EAN,
            'Nazwa': title,
            'Cena': price,
            'Kategoria': ' > '.join(categories),
        }
        for i in range(len(clean_keys)):
            result[clean_keys[i]] = clean_values[i]

        rates = response.css('div._6a940_3CAtl > span:nth-child(2) > span:nth-child(2)::text').getall()
        clean_rates = [int(rate) for rate in rates]
        result['Oceny'] = clean_rates

        rates_from_opinions = response.css('._6a940_30xl0 > div> div:nth-child(1) > div:nth-child(1) '
                                           '> div:nth-child(1) > span:nth-child(1) > meta:nth-child(1)').xpath(
            '@content').extract()
        clean_rates_up_from_opinions = [0 if x == '' else int(x) for x in rates_from_opinions]

        text_from_opinions = response.css('div._1t7v4::text').getall()

        thumbs_up_from_opinions = response.css('div._1dr57 > span:nth-child(1) > button:nth-child(1)').xpath(
            'string('                                                                                               '.)').extract()
        clean_thumbs_up_from_opinions = [0 if x == '' else x for x in thumbs_up_from_opinions]

        thumbs_down_from_opinions = response.css('div._1dr57 > span:nth-child(2) > button:nth-child(1)').xpath('string('
                                                                                                               '.)').extract()
        clean_thumbs_down_from_opinions = [0 if x == '' else x for x in thumbs_down_from_opinions]

        opinions_zipped = zip(clean_rates_up_from_opinions, text_from_opinions, clean_thumbs_up_from_opinions,
                              clean_thumbs_down_from_opinions)
        written_opinions = []
        for rate, text, thumbs_up, thumbs_down in opinions_zipped:
            data = dict()
            data['Ocena'] = rate
            data['Tekst'] = text
            data['Łapki w górę'] = thumbs_up
            data['Łapki w dół'] = thumbs_down
            written_opinions.append(data)

        result['Opinie'] = written_opinions
        yield result
